package org.bitbucket.beatngu13.scalacheck

import java.util.StringTokenizer

import org.scalacheck._

import scala.util.Random

/**
 * ScalaCheck demo for the viva voce exam of model-based software development.
 * 
 * @author danielkraus1986@gmail.com
 */
object Demo {
  
  /**
   * {@code TokenCounter} instance.
   */
  var tokenCounter = new TokenCounter
  /**
   * {@code StringGenerator} instance.
   */
  var stringGenerator = new StringGenerator

  def main(args: Array[String]) = {
	  demo1
//    demo2  
//    demo3
//    demo4(42)
    
//    for (i <- 1 to 10) println("Random string #" + i + ":\n" 
//        + stringGenerator.randomString(42) + "\n")
  }
  
  /**
   * Test oracle for token counting based on Scala library methods.
   */
  def oracle(input: String) = input.split(" +").count(_ != "")
  
  /**
   * Usage of arbitrary (standard) generator.
   */
  def demo1 = {
    val propCount = Prop.forAll {
      (input: String) => tokenCounter.count(input) == oracle(input)
    }

    propCount.check
  }
  
  /**
   * Collect input.
   */
  def demo2 = {
    val propCount = Prop.forAll {
      (input: String) => {
        Prop.collect(input) {
          tokenCounter.count(input) == oracle(input)
        }
      }
    }

    propCount.check
  }
  
  /**
   * Limit input to alphanumeric strings.
   */
  def demo3 = {
    val alphaStr = Gen.alphaStr
    
    val propCount = Prop.forAll(alphaStr) {
      (input: String) => {
        Prop.collect(input) {
          tokenCounter.count(input) == oracle(input)
        }
      }
    }

    propCount.check
  }
  
  /**
   * Add a random number of blanks and shuffle the whole string.
   */
  def demo4(maxLength: Int) = {
    val strGen = for {
      rdm <- Gen.choose(0, maxLength)
      blk <- Gen.const(" " * rdm)
      str <- Gen.alphaStr.retryUntil(_.size <= maxLength - rdm)   
    } yield (blk, str)
//    var halfLength = maxLength / 2
    
    val propCount = Prop.forAll(strGen) {
      case (blk, str) => {
        var word = Random.shuffle((blk + str).toList).mkString
        
        Prop.collect(word) {
//        Prop.classify(word.length() > halfLength, "greater than " + halfLength, 
//            "less than or equal to " + halfLength) {
          tokenCounter.count(word) == oracle(word)
        }
      }
    }

    propCount.check
  }

}