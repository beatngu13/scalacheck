package org.bitbucket.beatngu13.scalacheck

/**
 * Counts tokens which are separated by an arbitrary amount of whitespace 
 * characters.
 * 
 * @author danielkraus1986@gmail.com
 */
class TokenCounter {
  
  /**
   * Counts tokens. For instance, {@code " foo   b a  r"} will return 4.
   * 
   * @param Input The String to search for tokens.
   * @return The number of tokens.
   */
  def count(input: String): Int = {
    var wordCount = 0
    var isWord = false
    
    for (char <- input) {
      if (char == ' ') {
        isWord = false
      } else if (!isWord) {
        isWord = true
        wordCount += 1
      }
    }
    
    wordCount
  }
  
}