package org.bitbucket.beatngu13.scalacheck

import org.scalacheck._

import scala.util.Random

import Prop.forAll

/**
 * Test specification for {@link TokenCounter#count(String)}.
 * 
 * @author danielkraus1986@gmail.com
 */
object CountSpecification extends Properties("String") {
  
  /**
   * {@code TokenCounter} instance.
   */
  var tokenCounter = new TokenCounter
  
  /**
   * Test oracle for token counting based on Scala library methods.
   */
  def oracle(input: String) = input.split(" +").count(_ != "")
  
  /**
   * Test against arbitrary generator.
   */
  property("arbitraryCountTest") = forAll {
    (input: String) => tokenCounter.count(input) == oracle(input)
  }
  
  /**
   * Test against alphanumeric string generator.
   */
  property("alphaStrCountTest") = forAll(Gen.alphaStr) {
    (input) => tokenCounter.count(input) == oracle(input)
  }
  
  /**
   * Test against custom generator.
   */
  property("customCountTest") = forAll(
      for {
        rdm <- Gen.choose(0, 42)
        blk <- Gen.const(" " * rdm)
        str <- Gen.alphaStr.retryUntil(_.size <= 42 - rdm)   
      } yield (blk, str)  
    ) {
    case (blk, str) => {
      var word = Random.shuffle((blk + str).toList).mkString
      
      tokenCounter.count(word) == oracle(word)
    }  
  }
  
  // ...
  
}