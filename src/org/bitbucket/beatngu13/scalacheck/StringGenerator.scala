package org.bitbucket.beatngu13.scalacheck

import scala.util.Random

/**
 * Creates random strings based on a given alphabet and a bunch of whitespace 
 * characters.
 * 
 * @author danielkraus1986@gmail.com
 */
class StringGenerator {
  
  /**
   * The basic alphabet (-> letters).
   */
  var alphabet = 
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
  /**
   * The whitespace characters (-> blanks). Each value is listed as a String to 
   * avoid casts during further processing.
   */
  var whitespaces = List(" ", "\n", "\r", "\t")
  
  /**
   * Creates a random string (-> words + strings).
   * 
   * @param maxLength Desired maximum string length.
   * @return A random string.
   */
  def randomString(maxLength: Int): String = {
    // Actual length of the strings which is used temporary.
    var tempLength = Random.nextInt(maxLength + 1)
    // Maximum occurrence of each whitespace character.
    var occurence = tempLength / 4
    // String builder with the given initial capacity and value.
    var builder = new StringBuilder(tempLength, alphabet)
    
    // Add a random amount of whitespace characters to the alphabet.
    whitespaces.foreach(c => builder.append(c * Random.nextInt(
        if (occurence > 0) occurence else 1)))
    
    // Create the random string.
    Stream.continually(Random.nextInt(builder.length)).map(builder).take(
        tempLength).mkString
  }

}